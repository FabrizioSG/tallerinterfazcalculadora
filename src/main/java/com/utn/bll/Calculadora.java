/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utn.bll;

/**
 *
 * @author fabri
 */
public class Calculadora {
    private double resultado;
    
    public Calculadora(){
        this.resultado = 0;
    }
    public double realizarOperacion(double pOperando1, double pOperando2, String operacion){
        switch(operacion){
            case "Suma":
                this.resultado = pOperando1 + pOperando2;
                break;
            case "Resta":
                this.resultado = pOperando1 - pOperando2;
                break;
            case "Division":
                this.resultado = pOperando1/pOperando2;
                break;
            case "Multiplicacion":
                this.resultado = pOperando1*pOperando2;
                break;
        }
        
        return this.resultado;
    }
    
}
